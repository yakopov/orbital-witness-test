"""
Orbital Witness code challenge

Author: Yuriy Akopov (akopov@hotmail.co.uk)
Date: 2021-01-18
"""

import unittest
from unittest.mock import patch

import parser


class TestParser(unittest.TestCase):
    def test_parse_first_line(self):
        with self.assertRaises(ValueError):
            # the simplest test
            parser.parse_first_line("foobar")

        with self.assertRaises(ValueError):
            # too few columns
            parser.parse_first_line("01.01.2020   01.01.2000 FOO123")

        with self.assertRaises(ValueError):
            # too many columns
            parser.parse_first_line("01.01.2020  Foo 01.01.2000 FOO123 Bar")

        with self.assertRaises(ValueError):
            # dates in wrong format
            parser.parse_first_line("2020.01.01  Foo 1.1.200 FOO123")

        with self.assertRaises(ValueError):
            # title containes unexpected characters
            parser.parse_first_line("2020.01.01  Foo 1.1.200 FOO(1345")

        result = parser.parse_first_line(
            "01.01.2020  Foo  double  spaces    here 01.01.2000   TITLE123")
        self.assertDictEqual(result, {'reg_date': 0,
                                     'desc': 12,
                                     'lease_date': 40,
                                     'title_no': 53},
                                     "Offsets wrongs for double spaces in text")

        # TODO: this is not a complete set of tests, but I'll stop here because
        # the principle is clear and covering every edge case takes time

        # finally, try a real line
        result = parser.parse_first_line(
            "28.01.2009      Transformer Chamber (Ground   23.01.2009      EGL551039  ")
        self.assertDictEqual(result, {'reg_date': 0,
                                     'desc': 16,
                                     'lease_date': 46,
                                     'title_no': 62},
                                     "Offsets wrong for a real string")

    def test_get_note(self):
        self.assertIsNone(parser.get_note(""),
                          "Note detected in an empty string")
        self.assertIsNone(parser.get_note("NOTE:foobar"),
                          "Note detected when not separated with a space")

        self.assertEqual(parser.get_note("NOTE: Foo bar"), "Foo bar",
                         "Note not detected")

    def test_sort_dict(self):
        result = parser.sort_dict({
            'foo': 20,
            'bar': 10,
            'foobar': 100
        })

        self.assertEqual(len(result), 3)
        self.assertEqual(result[0], ('bar', 10))
        self.assertEqual(result[1], ('foo', 20))
        self.assertEqual(result[2], ('foobar', 100))

        # not checking the use case with two identical values as this is not our
        # expected use case and we will throw an exception if it happens anyway

    @patch('parser.parse_first_line')
    def test_parse_entry_text_exceptions(self, mock_parse_first_line):
        # testing function sanity checks that normally shouldn't be triggered
        mock_parse_first_line.return_value = {
            'reg_date': 0, 'foo': 4}

        with self.assertRaisesRegex(ValueError, "empty"):
            parser.parse_entry_text(["foo bar", ""])

        with self.assertRaisesRegex(ValueError, "not aligned"):
            parser.parse_entry_text(["foobar", ""])

        mock_parse_first_line.return_value = {
            'reg_date': 0, 'foo': 10, 'bar': 10, 'foobar': 20}

        with self.assertRaisesRegex(ValueError, "Non-unique"):
            parser.parse_entry_text(["foo", "bar"])

    # the following tests are real text lines that failed to parse on some
    # stage of development
    def test_parse_entry_text_no_note(self):
        entry_text = [
            "28.01.2009      Transformer Chamber (Ground   23.01.2009      EGL551039  ",
            "tinted blue     Floor)                        99 years from              ",
            "(part of)                                     23.1.2009"
        ]

        result = parser.parse_entry_text(entry_text)

        expected = {
            'note': None,       # note would be inserted first
            'reg_date': "28.01.2009 tinted blue (part of)",
            'desc': "Transformer Chamber (Ground Floor)",
            'lease_date': "23.01.2009 99 years from 23.1.2009",
            'title_no': "EGL551039",
        }

        self.assertDictEqual(result, expected,
                        "Failed to parse text without a note line")

    def test_parse_entry_text_note(self):
        entry_text = [
            "22.02.2010      Flat 2308 Landmark West       03.02.2010      EGL568130  ",
            "Edged and       Tower (twenty third floor     999 years from             ",
            "numbered 4 in   flat)                         1.1.2009                   ",
            "blue (part of)                                                           ",
            "NOTE: See entry in the Charges Register relating to a Deed of Rectification dated 26 January 2018"
        ]

        expected = {
            'note': "See entry in the Charges Register relating to a Deed of Rectification dated 26 January 2018",
            'reg_date': "22.02.2010 Edged and numbered 4 in blue (part of)",
            'desc': "Flat 2308 Landmark West Tower (twenty third floor flat)",
            'lease_date': "03.02.2010 999 years from 1.1.2009",
            'title_no': "EGL568130"
        }

        result = parser.parse_entry_text(entry_text)

        self.assertDictEqual(result, expected,
                        "Failed to parse text with a note line")

    def test_parse_entry_text_short_1(self):
        entry_text = [
            "08.01.2010      Parking Space 80 Landmark     12.11.2009      EGL565724  ",
            "Edged and       West Tower (basement level)   999 years from             ",
            "numbered 6 in                                 1.1.2009                   ",
            "blue on                                                                  ",
            "supplementary                                                            ",
            "plan 1"]       # this short dangling line used to break the parser1

        expected = {
            'note': None,
            'reg_date': "08.01.2010 Edged and numbered 6 in blue on supplementary plan 1",
            'desc': "Parking Space 80 Landmark West Tower (basement level)",
            'lease_date': "12.11.2009 999 years from 1.1.2009",
            'title_no': "EGL565724"
        }

        result = parser.parse_entry_text(entry_text)

        self.assertDictEqual(result, expected,
                        "Failed to parse text with a short line")




if __name__ == "__main__":
    unittest.main()
