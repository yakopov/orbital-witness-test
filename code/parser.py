"""
Orbital Witness code challenge

Author: Yuriy Akopov (akopov@hotmail.co.uk)
Date: 2021-01-18
"""

import typing
import re
import json
import argparse


def parse_first_line(line: str) -> list:
    """
    Parses the first line of the entry text and returns character count
    offsets that tell where the columns start.
    """
    # here we assume that the date is always in the same format and that all
    # 4 columns will always be present in the input (as it's the first line)

    # regexp is compiled even though it doesn't really affect performace
    # for two reasons - first is to separate the expression and the date,
    # second is to make it easier to write it in verbose mode as below
    # https://regex101.com is great for designing expressions like this
    line_exp = re.compile(r"""
             # Reg date: DD.MM.YYYY date followed by one or more spaces
             ^(?P<reg_date>\d{2}\.\d{2}\.\d{4})\s+
             # Property description: at least one non-space required, then space(s)
             (?P<desc>\S.+)\s+
             # Lease from date: another DD.MM.YYYY date, then space(s)
             (?P<lease_date>\d{2}\.\d{2}\.\d{4})\s+
             # Title number: all caps and digits title followed or not by spaces
             (?P<title_no>[A-Z0-9]+)\s*$
             """, re.VERBOSE)

    result = line_exp.fullmatch(line)

    if not result:
        raise ValueError(
            "Line \"{}\" failed to be parsed into 4 columns".format(line))

    # TODO: here we may add more sanity checks, e.g. to ensure that DD.MM.YYYY
    # substrings recovered as indeed dates (i.e. that there is no month 15 or
    # day 46, but it is excessive for the purpose of this task and would also
    # have impact on performance on large datasets. Another idea is to validate
    # title number with a more detailed regexp than what is used above for it.

    # TODO: if regular expression method is too slow on large datasets we may
    # try an assumption that columns in the first line are always separated by
    # at least two spaces which may or may not be true, but if it is, line can
    # parsed cheaper (caveat: possible double space in property description)

    # regexp group names become keys in returned structure
    return {key: result.start(key) for key in result.groupdict().keys()}


def get_note(line: str) -> typing.Union[str, None]:
    """
    Returns meaningful part of the note if the prefix is as expected,
    otherwise returns nothing
    """
    # no need for a regular expression here
    note_label = "NOTE: "
    return line[len(note_label):] if line.startswith(note_label) else None


def sort_dict(x: dict) -> list:
    """
    Dictionaries keep key insertion order in Python 3.7+, but for our purpose
    it is still conveniend to turn it into a sorted list of key-value tuples
    because this way we will be able to do to the 'next key' without hard-coding
    the key order anywhere but as names groups in parse_first_line() regexp
    """
    return [(k, v) for k, v in sorted(x.items(), key=lambda item: item[1])]


def parse_entry_text(lines: list) -> dict:
    """
    Parses a single entry from Schedule of notices of leases from a list of
    formatted human-redable lines into a structured dictionary
    """
    # A sample of what lines look like - each column may have different height,
    # all 4 guaranteed only on the first line. Last line with a note is not
    # a part of any column and is a separate
    # (and optional) field
    """
    DD.MM.YYYY      Property address       MM.DD.YYYY      TITLENUMBER
    How to find     and other location     X years from
    on the plan,    description here       D.M.YYYY
    e.g. colour
    NOTE: Single (?) line note
    """
    if len(lines) == 0:
        raise ValueError("Non-empty list of text lines expected")

    offsets = parse_first_line(lines[0])

    # sanity checks:
    # registration date is always the first one
    if offsets['reg_date'] != 0:
        raise ValueError(
            "Registration date offset is not 0 in line {}".format(lines[0]))
    # offset values should be unique
    if len(set(offsets.values())) != len(offsets):
        raise ValueError(
            "Non-unique offset values in line {}: {}".format(lines[0], offsets))

    # sort offset numbers so we can parse from the first to the last
    # dictionaries preserve insertion order of keys in Python 3.7+, but let's
    # still rebuild it as tuples as it'd be more convenient below
    offsets_sorted = sort_dict(offsets)

    # prepare returned structure
    result = {
        'note': None
    }

    # loop through lines with offsets we have determined
    for line_no, line in enumerate(lines):
        if (line_no + 1) == len(lines):
            # it is the last line which may contain a note
            note = get_note(line)
            if note is not None:
                result['note'] = note
                break

        # sanity check - line should also not be empty
        if len(line) == 0:
            raise ValueError("Line {} is empty".format(line))

        # if we're here it's either not the last line or the last one w/o a note
        for idx, (offset_key, offset_pos) in enumerate(offsets_sorted):
            # lines other than the first one are allowed to be shorter
            # no need to check the length of the first one though because
            # it would fail to parse if it's too short for 4 columns
            if offset_pos > len(line):
                continue

            if (offset_pos > 0) and (line[offset_pos - 1] != ' '):
                raise ValueError(
                    "Line {} not aligned with the first one".format(line))

            # finally, determine the column boundaries and cut it out
            if (idx + 1) == len(offsets_sorted):
                next_offset = len(line)
            else:
                next_offset = offsets_sorted[idx + 1][1]

            column_line_text = line[offset_pos:next_offset].strip()
            # check if it was just a stub
            if len(column_line_text) == 0:
                continue

            # assign it to the result structure
            if offset_key not in result:
                result[offset_key] = []

            result[offset_key].append(column_line_text)

    # glue strings
    for key, value in result.items():
        if isinstance(result[key], list):
            result[key] = " ".join(value)

    return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Orbital Witness coding challenge")
    parser.add_argument('--input_json', metavar="PATH", type=str, required=True, default=None,
                        help="Path to input JSON")
    parser.add_argument('--output_json', metavar="PATH", type=str, required=True, default=None,
                        help="Path to output JSON")

    args = parser.parse_args()

    print("Reading input from {}...".format(args))
    with open(args.input_json, 'r') as fh:
        input = json.load(fh)

    print("Parsing...")

    parsed = []
    failed = []
    for item in input:
        for entry in item['leaseschedule']['scheduleEntry']:
            entry_text = entry['entryText']

            try:
                parsed.append(parse_entry_text(entry_text))

            except Exception as er:
                print("******")
                print("{}: {}".format(str(type(er)), er))
                # convert everything to strings before printing to avoid type errors
                print("> " + "\n> ".join([str(line) for line in entry_text]))
                failed.append(entry_text)

    print("Parsed {} entries".format(len(parsed)))
    print("Failed to parse {} entries".format(len(failed)))

    print("Saving output to {}...".format(args.output_json))
    with open(args.output_json, 'w') as fh:
        json.dump(parsed, fh)

    print("Finished")
