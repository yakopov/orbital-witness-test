### About

Coding challenge for Orbital Witness:

* `code`
    - `parser.py` - solution script
    - `test_parser.py` - unit tests
* `data`
    - `Official_Copy_Register_EGL363613.pdf` - input data in PDF (not supported)
    - `parsed.json` - output of `parser.py`
    - `schedule_of_notices_of_lease_examples.json` - input data in JSON
* `Orbital_Witness_Candidate_Coding_Exercise_Python.pdf` - information about this task
* `README.md` - this document

### System requirements

Python 3.5+ (built and tested in Python 3.8), no additional modules.

### Usage

To parse a JSON file:

    python code/parser.py --input_json=data/schedule_of_notices_of_lease_examples.json --output_json=data/parsed.json

To run tests:

    python code/test_parser.py

### Items that fail to parse

Some of the items in the input JSON document fail the current version the parser: 242
out of 1766, or 13.7%. While it is possible to extend the parser to cover these
cases as well, this where the line for the first version is drawn.

Some of these use cases look legitimate, and some might be invalid input, so it might be
expected for the parser to signal that by raising alarm.

The failing entries can be split into several groups:

#### Case 1

Multiple NOTE lines - I only noticed that there might be more than one note when
I first processed the whole dataset as this is not immediately visible from the top few items.
Not fixed in the current version is it takes additional time:

    [
        ...,
        "NOTE 1: Copy  lease filed.",
        "NOTE 2: The lease dated 3 April 2001 referred to above has been determined as to part of the first floor as mentioned in the Deed of the "First Space" dated 20 December 2016 referred to in the Charges Register.",
        "NOTE 3: The lease dated 3 April 2001 referred to above has been determined as to part of the second floor as mentioned in the Deed of the "Second Space" dated 20 December 2016 referred to in the Charges Register.",
        "NOTE 4: The lease dated 3 April 2001 referred to above has been determined as to part of the second floor as mentioned in the Deed of the "Third Space" dated 30 April 2017 referred to in the Charges Register."
    ]

Sub-case: singe NOTE, but on more than one line:

    [
        "20.12.2002      92 Boydell Court (Fourth      06.12.2002      NGL816726  ",
        "1 (part of)     Floor Flat)                   From 6.12.2002             ",
        "to 25.3.2099               ",
        "NOTE: The lease was granted under the provisions of section 56 or 93",
        "(4) of the Leasehold Reform, Housing and Urban Development Act 1993"
    ]

#### Case 2

Most typical use case - no title number column which in this version is considered
mandatory. In case data without title number still makes business sense, this bit is
easy to fix:

    [
        "16.07.1993      3A Prince of Wales Road       18.06.1993      29.9.1992  ",
        "1 (part of):    (First Floor Maisonette)      99 years from              ",
        "29.9.1992                  ",
        "NOTE: The lease grants and reserves rights of passage and running of water, soil, gas, electricity, support and protection.  The lease grants rights of entry for repair and alteration, right of way on foot over the common pathway and a right to keep one dustbin in the dustbin area.  The lease reserves rights of entry for repair and maintenance of the lower maisonette."
    ]

#### Case 3

Same as above, but the column missing is registration date (the first one instead of the last):

    [
        "98 Kensington High Street     29.03.1973      NGL224837  ",
        "99 years (less             ",
        "3 days) from               ",
        "14/05/1952                 ",
        "NOTE: See entry in the Charges Register relating to a Deed of Variation dated 6 April 1998"
    ]

#### Case 5

Misaligned formatting - columns are not completely vertical. Can be solved by
re-parsing every line and not just the first one, but with a different regular
expression for lines other that the first:

    [
        "29.01.2007      1 Mancroft (Ground Floor)     02.01.2007      NGL875439  ",
        "and Garage 11 (Ground         From 2.1.2007              ",
        "Floor)                        to 21.11.2174              ",
        "NOTE: The lease was made under the provisions of section 56 or 93(4) of the Leasehold Reform Housing and Urban Development Act 1993"
    ]

#### Case 6

This is a unique entry as has a null value separating its note line. It is the
only occasion, so I did not add support for it, even though it is easy as this might
be a legitimate error.

This entry also seems to be missing from the PDF version
of input data (searching for its title number fails), so I could not check how it
appeared there:

    [
        "28.02.2014      98 Boydell Court (Sixth       14.02.2014      NGL940837  ",
        "3(part of)      Floor Flat)                   from and                   ",
        "including                  ",
        "14.02.2014 and             ",
        "expiring on                ",
        "and including              ",
        "19.03.2146",
        null,
        "NOTE: The lease was made under the provisions of section 56 or 93(4) of the Leasehold Reform, Housing and Urban Development Act 1993"
    ]
